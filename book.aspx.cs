﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace project1
{
    public partial class book : System.Web.UI.Page
    {
        string str = "Data Source=DESKTOP-J98SMJN\\SQLEXPRESS;Initial Catalog=project1;Integrated Security=True";
        admindataclassesDataContext dc;
        protected void Page_Load(object sender, EventArgs e)
        {
            dc = new admindataclassesDataContext(str);

           
       if(!IsPostBack)
       { 

            pubname.DataSource = dc.publications.ToList();
            pubname.DataTextField= "pname";
           
            pubname.DataBind();


            bchname.DataSource = dc.branches.ToList();
            bchname.DataTextField = "bname";
            bchname.DataValueField = "bid";
            bchname.DataBind();
        }

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            dc = new admindataclassesDataContext(str);
            

            book b = new book();
            b.bkname = bukname.Text;
            b.author=auth.Text;
            b.price = Convert.ToInt32(pice.Text);
            b.publication = pubname.SelectedItem.Text;
            b.branch = bchname.SelectedItem.Text;
            b.availquant = Convert.ToInt32(quant.Text);
            DateTime now = System.DateTime.Now;
            b.entrydate = Convert.ToDateTime(now);
            b.validquant = Convert.ToInt32(quant.Text)-1;
            dc.books.InsertOnSubmit(b);
            dc.SubmitChanges();
            //publication p = dc.publications.FirstOrDefault(x => x.pname.Equals(pubname.SelectedItem.Text));
            
            //branch br = dc.branches.FirstOrDefault(x => x.bname.Equals(bchname.SelectedItem.Text));
            GridView1.DataSource = dc.books.ToList();
            GridView1.DataBind();
           
            

            Response.Write("book is added");

        }

        protected void Button11_Click(object sender, EventArgs e)
        {
            dc = new admindataclassesDataContext(str);
            var books = (from x in dc.books select new {x.bkid, x.bkname, x.author, x.price, x.publication, x.branch, x.availquant });
            GridView1.DataSource = books;
            GridView1.DataBind();
        }

        protected void Button12_Click(object sender, EventArgs e)
        {
            dc = new admindataclassesDataContext(str);
            book b = dc.books.FirstOrDefault(x => x.bkname.Equals(bukname.Text));
            issuebuk i = new issuebuk();
            b.author = auth.Text;
            b.price = Convert.ToInt32(pice.Text);
            b.publication = pubname.SelectedItem.Text;
            b.branch = bchname.SelectedItem.Text;
            
           
            
            b.availquant = Convert.ToInt32(quant.Text);
            b.validquant = Convert.ToInt32(quant.Text)-1;
            dc.SubmitChanges();
            //foreach(var  x in dc.books)
            //{
            //    x.availquant--;
            //    x
            //}
            string bname = bukname.Text;
            var query = (from x in dc.books where x.bkname == bname select new { x.bkid, x.bkname, x.author,x.price,x.publication,x.branch,x.availquant,x.validquant });

            GridView1.DataSource = query;
            GridView1.DataBind();
        }

        protected void bukname_TextChanged(object sender, EventArgs e)
        {
            dc = new admindataclassesDataContext(str);
            book b = dc.books.FirstOrDefault(x => x.bkname.Equals(bukname.Text));
            if (b != null)
            {
                auth.Text = b.author.ToString(); ;
                pice.Text=b.price.ToString();
                pubname.SelectedItem.Text=b.publication.ToString();
                bchname.SelectedItem.Text=b.branch.ToString();
               quant.Text=b.availquant.ToString();
            }
            else
            {
                Response.Write("not exist");

            }



            dc.SubmitChanges();
            
        }

        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            dc = new admindataclassesDataContext(str);
            int bid = Convert.ToInt32(GridView1.DataKeys[e.RowIndex].Values[0]);
            book b = dc.books.FirstOrDefault(x => x.bkid.Equals(bid));
            dc.books.DeleteOnSubmit(b);


            dc.SubmitChanges();


            GridView1.DataSource = dc.books.ToList();
            GridView1.DataBind();
        }
    }
}