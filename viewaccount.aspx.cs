﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace project1
{
    public partial class viewaccount : System.Web.UI.Page
    {
        string str = "Data Source=DESKTOP-J98SMJN\\SQLEXPRESS;Initial Catalog=project1;Integrated Security=True";
        admindataclassesDataContext dc;
        protected void Page_Load(object sender, EventArgs e)
        {
            dc = new admindataclassesDataContext(str);
            string s = Session["username"].ToString();
            var q = (from x in dc.student1s where x.username == s select new { x.name, x.age, x.branch, x.city, x.state, x.pin, x.contact });
            DetailsView1.DataSource = q;
            DetailsView1.DataBind();
        }
    }
}