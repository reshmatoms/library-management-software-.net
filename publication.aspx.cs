﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace project1
{
    public partial class publication : System.Web.UI.Page
    {
        //SqlConnection con;
        //SqlCommand cmd;
        //SqlDataReader rd;
        string str = "Data Source=DESKTOP-J98SMJN\\SQLEXPRESS;Initial Catalog=project1;Integrated Security=True";
        admindataclassesDataContext dc;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Buttonpub_Click(object sender, EventArgs e)
        {
            dc = new admindataclassesDataContext(str);
            publication p = new publication();
            p.pid = Convert.ToInt32(pubid.Text);
            p.pname = pubname.Text;
            DateTime now = System.DateTime.Now;  
            p.entrydate = Convert.ToDateTime(now);
            dc.publications.InsertOnSubmit(p);
            dc.SubmitChanges();

            int pid = Convert.ToInt32(pubid.Text);
            var query = (from x in dc.publications where x.pid == pid select new { x.pid, x.pname, x.entrydate });

            GridView1.DataSource =query;
            GridView1.DataBind();
            RequiredFieldValidator1.Enabled = true;
            RequiredFieldValidator2.Enabled = true;
            
            //    pubid.Text = "";
            //pubname.Text = "";

        }

        //protected void Button11_Click(object sender, EventArgs e)
        //{
        //    dc = new admindataclassesDataContext(str);
        //    publication p = dc.publications.FirstOrDefault(x => x.pid.Equals(pubid.Text));
        //    dc.publications.DeleteOnSubmit(p);
        //    dc.SubmitChanges();
          
        //        Response.Write("deleted");
           
            
        //    var query = (from x in dc.publications select new { x.pid, x.pname,x.entrydate });
            
        //    GridView1.DataSource = query;
        //    GridView1.DataBind();

        //}

        protected void pubid_TextChanged(object sender, EventArgs e)
        {
            dc = new admindataclassesDataContext(str);
            publication p = dc.publications.FirstOrDefault(x => x.pid.Equals(pubid.Text));
           if(p!=null)
            {
               pubname.Text = p.pname.ToString(); 
           }
           else
           {
               Response.Write("not exist");
            
           }

           
        
            dc.SubmitChanges();
            
        }

        protected void Button12_Click(object sender, EventArgs e)
        {
            dc = new admindataclassesDataContext(str);
            publication p = dc.publications.FirstOrDefault(x => x.pid.Equals(pubid.Text));
            p.pname = pubname.Text;
            dc.SubmitChanges();
            int pid=Convert.ToInt32(pubid.Text);
            var query = (from x in dc.publications where x.pid==pid select new { x.pid, x.pname ,x.entrydate});
            
            GridView1.DataSource = query;
            GridView1.DataBind();
            RequiredFieldValidator1.Enabled = true;
            RequiredFieldValidator2.Enabled = true;


        }

        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            dc = new admindataclassesDataContext(str);
            int pid = Convert.ToInt32(GridView1.DataKeys[e.RowIndex].Values[0]);
            publication p = dc.publications.FirstOrDefault(x => x.pid.Equals(pid));
            dc.publications.DeleteOnSubmit(p);
            
           
            dc.SubmitChanges();


            GridView1.DataSource = dc.publications.ToList() ;
            GridView1.DataBind();
            Labelmsg.Text = "row is deleted";
            
        }

        protected void Button14_Click(object sender, EventArgs e)
        {
            dc = new admindataclassesDataContext(str);
            GridView1.DataSource = dc.publications.ToList();
            GridView1.DataBind();

        }

       

        
    }
}