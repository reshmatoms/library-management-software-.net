﻿<%@ Page Title="" Language="C#" MasterPageFile="~/STUDENTNESTED.master" AutoEventWireup="true" CodeBehind="editaccount.aspx.cs" Inherits="project1.editaccount" %>
<asp:Content ID="Content1" ContentPlaceHolderID="G" runat="server">
    <div style="background-color:#6b5c5c;">
       
    <center>
            <asp:Label ID="bookhead" runat="server" Text="EDIT ACCOUNT" Font-Bold="True" Font-Size="Large" ForeColor="#FFFFCC"></asp:Label>

        </center>
        </div>
    <div>
        <asp:Literal ID="Literal2" runat="server"></asp:Literal>
    </div>
    <table class="auto-style1">

               
                <tr>
                    <td class="auto-style9">
                        <asp:Label ID="name" runat="server" Text="Name:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="Textname" runat="server" ></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style5">
                        <asp:Label ID="branch" runat="server" Text="Branch:"></asp:Label>
                    </td>
                    <td class="auto-style6">
                        <asp:DropDownList ID="DropDownList1" runat="server">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style9">
                        <asp:Label ID="Contact" runat="server" Text="Contact:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="Textcontact" runat="server" TextMode="Number"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style9">
                        <asp:Label ID="city" runat="server" Text="City:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="Textcity" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style9">
                        <asp:Label ID="state" runat="server" Text="State"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="Textstate" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style9">
                        <asp:Label ID="pin" runat="server" Text="Pincode"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="Textpin" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style9">
                        <asp:Label ID="age" runat="server" Text="Age"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="Textage" runat="server"></asp:TextBox>
                    </td>
                </tr>
                
                <tr>
                    <td colspan="2">
                       <center> 
                           <br />
                           <br />
                           <asp:Button ID="Button5" runat="server" Text="EDIT" OnClick="Button5_Click" />
                           <asp:Literal ID="Literal1" runat="server"></asp:Literal>
                           <br />
</asp:Content>
