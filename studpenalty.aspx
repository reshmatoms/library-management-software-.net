﻿<%@ Page Title="" Language="C#" MasterPageFile="~/STUDENTNESTED.master" AutoEventWireup="true" CodeBehind="studpenalty.aspx.cs" Inherits="project1.studpenalty" %>
<asp:Content ID="Content1" ContentPlaceHolderID="G" runat="server">
    <div style="background-color:#6b5c5c;">
       
    <center>
            <asp:Label ID="issuehead" runat="server" Text="PENALTY" Font-Bold="True" Font-Size="Large" ForeColor="#FFFFCC"></asp:Label>

        </center>
        </div>
    <div style="height: 230px">

        <table class="auto-style1" style="height: 228px">
            <tr>
                <td style="width: 224px">
                    <asp:Label ID="Label3" runat="server" Text="STUDENT NAME"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="stdname" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="width: 224px">
                    <asp:Label ID="Label7" runat="server" Text="BOOK NAME"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="bookname" runat="server" OnSelectedIndexChanged="bookname_SelectedIndexChanged" >
                    </asp:DropDownList>
                    <asp:Button ID="Button11" runat="server" Text="VIEW" />
                </td>
            </tr>
            <tr>
                <td style="width: 224px">
                    <asp:Label ID="Label4" runat="server" Text="BRANCH"></asp:Label>
                </td>
                <td>
                &nbsp;<asp:TextBox ID="branch" runat="server"></asp:TextBox>
                    </td>
            </tr>
            <tr>
                <td style="width: 224px">
                    <asp:Label ID="Label9" runat="server" Text="ISSUE DATE"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="issue" runat="server"></asp:TextBox>
                    </td>
            </tr>
            <tr>
                <td style="width: 224px">
                    <asp:Label ID="Label10" runat="server" Text="RETURN DATE"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="ret" runat="server"></asp:TextBox>
                    </td>
            </tr>
            <tr>
                <td style="width: 224px">
                    <asp:Label ID="Label8" runat="server" Text="FINE"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="quant" runat="server"></asp:TextBox>
                </td>
            </tr>
            </table>

    </div>
    <div>
     <center>  
         <br />
         <br />
         &nbsp;&nbsp;&nbsp;&nbsp;<br />
         <br />
         &nbsp;&nbsp; 
         <asp:Literal ID="Literal1" runat="server"></asp:Literal>
        </center> 
    </div>
</asp:Content>
