﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace project1
{
    public partial class editaccount : System.Web.UI.Page
    {
        string str = "Data Source=DESKTOP-J98SMJN\\SQLEXPRESS;Initial Catalog=project1;Integrated Security=True";
        admindataclassesDataContext dc;
        protected void Page_Load(object sender, EventArgs e)
        {
            dc = new admindataclassesDataContext(str);
            if (!IsPostBack)
            {
                DropDownList1.DataSource = dc.branches.ToList();
                DropDownList1.DataTextField = "bname";
                DropDownList1.DataBind();
                DropDownList1.Items.Insert(0, new ListItem("--select--", "0"));

                Literal2.Text = "Enter ur name and click view to edit";
                Textname.Text = Session["username"].ToString();
                student1 u = dc.student1s.FirstOrDefault(x => x.name.Equals(Textname.Text));
                DropDownList1.SelectedItem.Text = u.branch.ToString();
                Textcontact.Text = u.contact.ToString();
                Textcity.Text = u.city.ToString();
                Textstate.Text = u.state.ToString();
                Textpin.Text = u.pin.ToString();
                Textage.Text = u.age.ToString();

            }
        }

        protected void Button5_Click(object sender, EventArgs e)
        {
            dc = new admindataclassesDataContext(str);
            string s = Session["username"].ToString();
            var q = (from x in dc.student1s where x.name == s select x);
            foreach (var x in q)
            {
                x.name = Textname.Text;
                x.branch = DropDownList1.SelectedItem.Text;
                x.contact = Convert.ToInt64(Textcontact.Text);
                x.city = Textcity.Text;
                x.state = Textstate.Text;
                x.pin = Convert.ToInt32(Textpin.Text);
                x.age = Convert.ToInt32(Textage.Text);
            }
            dc.SubmitChanges();
            Literal1.Text = "edited";
           
           
        }

       
    }
}