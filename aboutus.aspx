﻿<%@ Page Title="" Language="C#" MasterPageFile="~/STRT.master" AutoEventWireup="true" CodeBehind="aboutus.aspx.cs" Inherits="project1.aboutus" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ABC" runat="server">
    <center>   <h2>!!!!WELCOME TO LIBRARY!!!!</h2></center>
    <br />
    <h3 id="title-heading" style="margin: 0.25em 0px 0.5em;  font-size: 2rem;  ">Mission and Vision</h3>
    <article style="display: block; color: rgb(51, 51, 51);   font-size: 16px;      ">
       
        <section class="clearfix" style="display: block;">
            <h3  style="margin: 0.5em 0px 0.25em; font-size: 1.5rem;">Library Mission
               </h3>
            <span>
                The library offers a powerful combination of traditional and digital resources, 
                student-centered spaces, and a personal commitment to excellent service.&nbsp;
                As the social and academic hub of the college, the library is dedicated to
                the achievement and success of our students. &nbsp;
                Within an inclusive and supportive environment, the library provides agile services,
                information literacy instruction, learning and research opportunities, and preservation of 
                College as a part of the  University.</span><br />
            <h4  style="margin: 0.5em 0px 0.25em; font-size: 1.5rem; ">Library Vision</h4>
            <ul  font-size: 16px;">
                <li >build a more inclusive and transformative academic and social environment</li>
                <li >fully integrate the library’s role into the teaching, learning, technology, research, and personal enrichment of College</li>
                <li >innovate and exceed traditional library expectations as a vital teaching, research, and community resource</li>
                <li >develop and enrich collaborative digital learning experiences</li>
                <li >advance the goals of the College&nbsp; and the University through collaborative partnerships</li>
                <li >become a leader on the local, regional, and national levels by expanding our presence and services.</li>
            </ul>
            <div >
                <h3 >Library Timings</h3>
            </div>
            <div style=" font-size: 14px;">
                
                    <span>The Library currently kept open on all the working days, Saturday and Sunday,
                        except the institute holidays.<br />
                    </span><br /></div>
<div >
               <center>    <table style="height: 285px; width: 507px">
                        <tr >
                            <td colspan="2" style="background-color: #666666">
                                <div   >
                                    <b ><span style="font-size: 12pt; color: white;">Library Opening Hours</span></b></div>
                            </td>
                        </tr>
                        <tr>
                            <td >
                                <div >
                                    <span style="font-size: small;"><strong >Monday to Friday</strong></span></div>
                            </td>
                            <td >
                                <div >
                                    <span style="font-size: small;"><strong >8.30 AM to 10.00 PM</strong></span></div>
                            </td>
                        </tr>
                        <tr >
                            <td >
                                <div >
                                    <span style="font-size: small;"><strong >Saturday and Sunday</strong></span></div>
                            </td>
                            <td >
                                <div  >
                                    <span style="font-size: small;"><strong><span>8.30 AM to 5.30 pm</span></strong></span></div>
                            </td>
                        </tr>
                        <tr >
                            <td colspan="2" style="background-color: #666666" >
                                <div  >
                                    <b ><span style="font-size: 12pt; color: white;">Library Circulation Hours</span></b></div>
                            </td>
                        </tr>
                        <tr >
                            <td >
                                <div >
                                    &nbsp;<strong ><span style="font-size: small;">Monday to Friday</span></strong></div>
                            </td>
                            <td >
                                <div  >
                                    <strong ><span style="font-size: small;">9.00 AM to 9.30 PM</span></strong></div>
                            </td>
                        </tr>
                        <tr >
                            <td >
                                <div >
                                    <strong ><span style="font-size: small;">Saturday and Sunday</span></strong></div>
                            </td>
                            <td >
                                <div  >
                                    <strong ><span style="font-size: small;">8.30 AM to 5.30 PM</span></strong></div>
                            </td>
                        </tr>
                        <tr >
                            <td colspan="2" style="background-color: #666666" >
                                <div >
                                    <b ><span style="font-size: 12pt; color: white;">Reading Room Hours</span></b></div>
                            </td>
                        </tr>
                        <tr >
                            <td >
                                <div >
                                    &nbsp;<strong><span style="font-size: small;">Monday to Friday</span></strong></div>
                            </td>
                            <td >
                                <div >
                                    <strong ><span style="font-size: small;">8.30 AM to 10.00 PM</span></strong></div>
                            </td>
                        </tr>
                        <tr >
                            <td >
                                <div>
                                    <strong ><span style="font-size: small;">Saturday, Sunday, Holidays</span></strong></div>
                            </td>
                            <td>
                                <div >
                                    <strong ><span style="font-size: small;">8.30 AM to 5.30 PM</span></strong></div>
                            </td>
                        </tr>
                    </table>
                   </center>
                </div>
           
            <p>
                
                <asp:LinkButton ID="LinkButton1" runat="server" PostBackUrl="~/startpage.aspx" >BACK</asp:LinkButton>

                 </p>
            
           </section>
    </article>
</asp:Content>
