﻿<%@ Page Title="" Language="C#" MasterPageFile="~/nestedadmin.master" AutoEventWireup="true" CodeBehind="publication.aspx.cs" Inherits="project1.publication" %>
<asp:Content ID="Content1" ContentPlaceHolderID="e" runat="server">
    <div style="background-color:#6b5c5c;">
        <center>
            <asp:Label ID="pubhead" runat="server" Text="ADD NEW PUBLICATION" Font-Bold="True" Font-Size="Large" ForeColor="#FFFFCC"></asp:Label>

        </center>
    </div>
   <div>

       <br />
       <br />
       
       <table >
           <tr>
               <td  style="width: 507px">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Label ID="Label2" runat="server" Font-Bold="True" Text="PUBLICATION ID:"></asp:Label>
                </td>
               <td style="width: 489px">
                   <asp:TextBox ID="pubid" runat="server" Width="120px" OnTextChanged="pubid_TextChanged" ></asp:TextBox>
                                      <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="feild isempty" ControlToValidate="pubid" ForeColor="Red" Enabled="false"   ></asp:RequiredFieldValidator>

               </td>
           </tr>
           <tr>
               <td  style="width: 507px">
                   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                   <asp:Label ID="Label3" runat="server" Font-Bold="True" Text="PUBLICATION NAME:"></asp:Label>
               </td>
               <td style="width: 489px">
                   <asp:TextBox ID="pubname" runat="server"></asp:TextBox>
                                      <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="feild isempty" ControlToValidate="pubname" ForeColor="Red" Enabled="false"></asp:RequiredFieldValidator>

                   <asp:Button ID="Button13" runat="server" Text="view" />

               </td>
           </tr>
           <tr>
               <td colspan="2">
                   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                   <br />
&nbsp;&nbsp;
<center><asp:Button ID="Buttonpub" runat="server" Text="ADD PUBLICATION" OnClick="Buttonpub_Click"></asp:Button>
               &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                   <asp:Button ID="Button12" runat="server" OnClick="Button12_Click" style="height: 26px" Text="UPDATE PUBLICATION" />
                   &nbsp;&nbsp;&nbsp;&nbsp;<asp:Button ID="Button14" runat="server" OnClick="Button14_Click" Text="VIEW ALL PUBLICATIONS" />
    <br />
                   <asp:Label ID="Labelmsg" runat="server" Font-Bold="True" Font-Size="Large" ForeColor="Red"></asp:Label></center>
                   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                   <br />
                   <br />
                  <center> <asp:GridView ID="GridView1" runat="server" DataKeyNames="pid" AutoGenerateColumns="False" OnRowDeleting="GridView1_RowDeleting"  >
                      <Columns>
                          <%--<asp:BoundField  />--%>
                          <asp:TemplateField HeaderText="Publication ID">
                              <ItemTemplate>
                                  <asp:Label ID="Label2" runat="server" Font-Bold="True" Text='<%#Eval("pid") %>'></asp:Label>
                              </ItemTemplate>
                              <EditItemTemplate>
                                   <asp:TextBox ID="pubid" runat="server" Width="120px" Text='<%#Eval("pid") %>' ></asp:TextBox>
                           </EditItemTemplate>
                                 </asp:TemplateField>
                          <asp:TemplateField HeaderText="Publication Name">
                              <ItemTemplate>
                                  <asp:Label ID="Label3" runat="server" Font-Bold="True" Text='<%#Eval("pname") %>'></asp:Label>
                              </ItemTemplate>
                              <EditItemTemplate>
                                   <asp:TextBox ID="pubname" runat="server" Width="120px" Text='<%#Eval("pname") %>' ></asp:TextBox>
                              </EditItemTemplate>
                          </asp:TemplateField>
                          <%--<asp:BoundField  />--%>
                          <asp:CommandField ShowDeleteButton="True" ShowCancelButton="False" />
                      </Columns>
                   </asp:GridView></center>
               </td>
           </tr>
       </table>


   </div>
    <div>
        <br /><center>
        <br />
</center>
    </div>
</asp:Content>
