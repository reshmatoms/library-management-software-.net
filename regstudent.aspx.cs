﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace project1
{
    public partial class regstudent : System.Web.UI.Page
    {
        string str = "Data Source=DESKTOP-J98SMJN\\SQLEXPRESS;Initial Catalog=project1;Integrated Security=True";
        admindataclassesDataContext dc;
        protected void Page_Load(object sender, EventArgs e)
        {
            dc = new admindataclassesDataContext(str);
            if (!IsPostBack) {
            DropDownList1.DataSource = dc.branches.ToList();
            DropDownList1.DataTextField = "bname";
            DropDownList1.DataBind(); 
            }
        }

        protected void Textdob_TextChanged(object sender, EventArgs e)
        {
            DateTime agecal;
            DateTime now;
            int result;
            now = DateTime.Now;
            agecal = Convert.ToDateTime(Textdob.Text);
            result = now.Year - agecal.Year;
            Textage.Text = Convert.ToString(result);
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            dc = new admindataclassesDataContext(str);
            student1 u = new student1();
            //int i=(from x in dc.students select)
            //u.sid = Convert.ToInt32(Textid.Text);
            u.name = Textname.Text;
            u.branch = DropDownList1.SelectedItem.Text;
            u.contact = Convert.ToInt64(Textcontact.Text);

            u.city = Textcity.Text;
            u.state = Textstate.Text;
            u.pin = Convert.ToInt32(Textpin.Text);
            u.age = Convert.ToInt32(Textage.Text);
            u.gender = RadioButtonList1.SelectedItem.Text;
            u.username = Textuser.Text;
            u.password = Textpass.Text;
            dc.student1s.InsertOnSubmit(u);
            dc.SubmitChanges();
            GridView1.DataSource = dc.student1s.ToList();
            GridView1.DataBind();
            //Textid.Text = " ";
            Textname.Text = " ";
            DropDownList1.SelectedItem.Text = "";
            Textcontact.Text = " ";
            Textcity.Text = " ";
            Textstate.Text = " ";
            Textpin.Text = " ";
            Textage.Text = " ";
            RadioButtonList1.SelectedItem.Text = " ";
            Textuser.Text = " ";
            Textpass.Text = " ";





            
        }

        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            dc = new admindataclassesDataContext(str);
            int pid = Convert.ToInt32(GridView1.DataKeys[e.RowIndex].Values[0]);
            student1 p = dc.student1s.FirstOrDefault(x => x.sid.Equals(pid));
            dc.student1s.DeleteOnSubmit(p);


            dc.SubmitChanges();


            GridView1.DataSource = dc.student1s.ToList();
            GridView1.DataBind();
        }

       
    }
}