﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace project1
{
    public partial class bookreport : System.Web.UI.Page
    {
        string str = "Data Source=DESKTOP-J98SMJN\\SQLEXPRESS;Initial Catalog=project1;Integrated Security=True";
        admindataclassesDataContext dc;
        protected void Page_Load(object sender, EventArgs e)
        {
            dc = new admindataclassesDataContext(str);
            if(!IsPostBack)
            {
                var g = (from x in dc.books select x.branch).Distinct();
                DropDownList1.DataSource = g;

                DropDownList1.DataBind();
                //DropDownList1.DataSource = dc.books.ToList().Distinct();
                //DropDownList1.DataTextField = "branch";
                //DropDownList1.DataBind();
                var h = (from x in dc.books select x.publication).Distinct();
                DropDownList2.DataSource = h;

                DropDownList2.DataBind();

                //DropDownList2.DataSource = dc.books.ToList();
                //DropDownList2.DataTextField = "publication";
                //DropDownList2.DataBind();
                DropDownList1.Items.Insert(0, new ListItem("--select--", "0"));
                DropDownList2.Items.Insert(0, new ListItem("--select--", "0"));
            }

        }

        protected void Button11_Click(object sender, EventArgs e)
        {
            dc = new admindataclassesDataContext(str);


            string bchname = DropDownList1.SelectedItem.Text;
            string pbnname = DropDownList2.SelectedItem.Text;
            var query = (from x in dc.books where (x.branch == bchname && x.publication == pbnname) select new { x.bkname, x.price, x.availquant, x.validquant });
            GridView1.DataSource = query;
            GridView1.DataBind();

        }
    }
}