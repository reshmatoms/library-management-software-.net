﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="regstudent.aspx.cs" Inherits="project1.regstudent" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
  
     <style type="text/css">
        .myt
        { 
            color:#F5F5F5;
            width:450px;
            text-align:left;
            background-color:#00C6C6;
            border-radius:12px;
            opacity:80;
            padding:10px;
            font-size:medium;
            font-weight:500;
            font-family:'Palatino Linotype';
            
             
        }
        .conto input[type="text"],input[type="password"],input[type="label"]
        {
             border-radius:7px;
            border-style:groove;
            padding-left:15px;
            margin-top:15px;
            height:20px;
            box-shadow:0px 0px 5px #333;
            outline:none;
            border:1px solid #ffa853;
           

        }
        .conto input[type="submit"]{
            border-radius:8px;
            border-style:groove;
            font-size:medium;
            margin-top:15px;
            background-color:#2762f7;
            color:black;
            height:30px;
            width:100px;
        }
        .lab{
            margin-top:15px;
        }
          #lefts{
            width:1130px;
            float:left;
            display:block;
        } 
         
         .auto-style3 {
             width: 328px;
             height: 56px;
             text-align:center;
         }
         .auto-style5 {
             width: 172px;
             height: 31px;
         }
         .auto-style6 {
             height: 31px;
         }
         .auto-style1 {
             height: 611px;
             width: 878px;
         }
         .auto-style9 {
             width: 172px;
         }
         .auto-style10 {
             width: 172px;
             height: 40px;
         }
         .auto-style11 {
             height: 10px;
         }
         
         </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <div style="height: 989px; width: 1104px">
        
          <div id="lefts">
       <img src="images/download (1).jpg"style="align-self:flex-end; width: 1123px; height: 116px; " />
              </div>
     <div id="log" class="myt" style="width:1090px; display:block;float:left; height: 857px; margin-right: 0px;">


        <asp:Panel ID="Panel1" runat="server" Height="865px" Width="1067px">
            <table class="auto-style1">

                <tr>
                    <td class="auto-style3" colspan="2">
                       <center style="width: 841px"> <asp:Label ID="Label3" runat="server" Text="SIGN UP FOR A NEW ACCOUNT"></asp:Label>
                    </center></td>
                </tr>
                <%--<tr>
                    <td class="auto-style9">
                        <asp:Label ID="id" runat="server" Text="Student ID:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="Textid" runat="server" style="margin-left: 4px"></asp:TextBox>
                    </td>
                </tr>--%>
                <tr>
                    <td class="auto-style9">
                        <asp:Label ID="name" runat="server" Text="Name:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="Textname" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="Textname" ErrorMessage="feild is empty"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style5">
                        <asp:Label ID="branch" runat="server" Text="Branch:"></asp:Label>
                    </td>
                    <td class="auto-style6">
                        <asp:DropDownList ID="DropDownList1" runat="server">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style9">
                        <asp:Label ID="Contact" runat="server" Text="Contact:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="Textcontact" runat="server" TextMode="Number"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="Textcontact" ErrorMessage="feild is empty"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style9">
                        <asp:Label ID="city" runat="server" Text="City:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="Textcity" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="Textcity" ErrorMessage="feild is empty"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style9">
                        <asp:Label ID="state" runat="server" Text="State"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="Textstate" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="Textstate" ErrorMessage="feild is empty"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style9">
                        <asp:Label ID="pin" runat="server" Text="Pincode"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="Textpin" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="Textpin" ErrorMessage="feild is empty"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="Textpin" ErrorMessage="pin is not valid" ValidationExpression="[0-9]{6}"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style9">
                        <asp:Label ID="dob" runat="server" Text="DOB"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="Textdob" runat="server" AutoPostBack="True" OnTextChanged="Textdob_TextChanged" TextMode="Date"></asp:TextBox> 
                    </td>
                </tr>
                <tr>
                    <td class="auto-style9">
                        <asp:Label ID="age" runat="server" Text="Age"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="Textage" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style10">
                        <asp:Label ID="gender" runat="server" Text="Gender  "></asp:Label>
                    </td>
                    <td class="auto-style11">
                        <asp:RadioButtonList ID="RadioButtonList1" runat="server" AutoPostBack="True" RepeatDirection="Horizontal" Width="147">

                            <asp:ListItem Value="1">Male</asp:ListItem>
                            <asp:ListItem Value="2">Female</asp:ListItem>

                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="RadioButtonList1" ErrorMessage="select ur gender"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style9">
                        <asp:Label ID="username" runat="server" Text="Username"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="Textuser" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="Textuser" ErrorMessage="feild is empty"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style9">
                        <asp:Label ID="confuser" runat="server" Text="Confirm Username"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="Textconfuser" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="Textconfuser" ErrorMessage="feild is empty"></asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="Textuser" ControlToValidate="Textconfuser" ErrorMessage="specify ur correct uername"></asp:CompareValidator>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style9">
                        <asp:Label ID="pass" runat="server" Text="Password:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="Textpass" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="Textpass" ErrorMessage="feild is empty"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style9">
                        <asp:Label ID="Label12" runat="server" Text="Confirm Password"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="Textconpass" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ControlToValidate="Textconpass" ErrorMessage="feild is empty"></asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToCompare="Textpass" ControlToValidate="Textconpass" ErrorMessage="specify ur correct password"></asp:CompareValidator>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                       <center> 
                           <br />
                           <br />
                           <asp:Button ID="Button1" runat="server" BackColor="#0066FF" BorderColor="#000066" BorderStyle="Groove" Font-Bold="True" Font-Size="Medium"  Text="Register" OnClick="Button1_Click" />
                           <br />
                           <br />
                           <asp:GridView ID="GridView1" runat="server"  DataKeyNames="sid" OnRowDeleting="GridView1_RowDeleting" >
                               <Columns>
                                   <asp:CommandField ShowDeleteButton="True" />
                               </Columns>
                           </asp:GridView>
                    </center></td>
                </tr>
            </table>

        </asp:Panel>

    </div>
    </div>
    </form>
</body>
</html>
