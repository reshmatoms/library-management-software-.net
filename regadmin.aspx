﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="regadmin.aspx.cs" Inherits="project1.regadmin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <style type="text/css">
        .myt
        { 
            color:#F5F5F5;
            width:450px;
            text-align:left;
            background-color:#00C6C6;
            border-radius:12px;
            opacity:80;
            padding:10px;
            font-size:medium;
            font-weight:500;
            font-family:'Palatino Linotype';
            
             
        }
        .conto input[type="text"],input[type="password"],input[type="label"]
        {
             border-radius:7px;
            border-style:groove;
            padding-left:15px;
            margin-top:15px;
            height:20px;
            box-shadow:0px 0px 5px #333;
            outline:none;
            border:1px solid #ffa853;
           

        }
        .conto input[type="submit"]{
            border-radius:8px;
            border-style:groove;
            font-size:medium;
            margin-top:15px;
            background-color:#2762f7;
            color:black;
            height:30px;
            width:100px;
        }
        .lab{
            margin-top:15px;
        }
         .auto-style2 {
        width: 127px;
    }
          #lefts{
            width:1130px;
            float:left;
            display:block;
        } 
         
         </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        
          <div id="lefts">
       <img src="images/download (1).jpg"style="align-self:center stretch; width: 933px; height: 116px;" />
              </div>
     <div id="log" class="myt" style="width:660px; display:block;float:left; height: 400px;">


        <asp:Panel ID="Panel1" runat="server" Height="409px" Width="667px">
            <table class="auto-style1" style="height: 401px; width: 99%">

                <tr>
                    <td class="auto-style3" colspan="2">
                       <center> <asp:Label ID="Label3" runat="server" Text="SIGN UP FOR A NEW ACCOUNT"></asp:Label>
                    </center></td>
                </tr>
                <tr>
                    <td class="auto-style2">
                        <asp:Label ID="Username" runat="server" Text="Username"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="Textuser" runat="server" style="margin-left: 4px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="Textuser" ErrorMessage="feild is empty"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style2">
                        <asp:Label ID="Label8" runat="server" Text="userid"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="Textid" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="Textid" ErrorMessage="feild is empty"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style2">
                        <asp:Label ID="Label2" runat="server" Text="Password"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="Textpass" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="Textpass" ErrorMessage="feild is empty"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style2">
                        <asp:Label ID="Label4" runat="server" Text="Confirm Password"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="Textconfpwd" runat="server"></asp:TextBox>
                        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="Textpass" ControlToValidate="Textconfpwd" ErrorMessage="write correct password"></asp:CompareValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="Textconfpwd" ErrorMessage="feild is empty"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                       <center> <asp:Button ID="Button1" runat="server" BackColor="#0066FF" BorderColor="#000066" BorderStyle="Groove" Font-Bold="True" Font-Size="Medium" OnClick="Button1_Click" Text="Register" />
                    </center></td>
                </tr>
            </table>

        </asp:Panel>

    </div>
    </div>
    </form>
</body>
</html>
