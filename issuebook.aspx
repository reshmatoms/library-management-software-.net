﻿<%@ Page Title="" Language="C#" MasterPageFile="~/nestedadmin.master" AutoEventWireup="true" CodeBehind="issuebook.aspx.cs" Inherits="project1.issuebook" %>
<asp:Content ID="Content1" ContentPlaceHolderID="e" runat="server">
    <div style="background-color:#6b5c5c;">
       
    <center>
            <asp:Label ID="issuehead" runat="server" Text="ISSUE BOOK" Font-Bold="True" Font-Size="Large" ForeColor="#FFFFCC"></asp:Label>

        </center>
        </div>
    <div style="height: 230px">

        <table class="auto-style1" style="height: 228px">
            <tr>
                <td style="width: 312px">
                    <asp:Label ID="Label2" runat="server" Text="STUDENT ID"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="DropDownList1" runat="server" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged" AutoPostBack="True">
                    </asp:DropDownList>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </td>
            </tr>
            <tr>
                <td style="width: 312px">
                    <asp:Label ID="Label3" runat="server" Text="STUDENT NAME"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="stdname" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="width: 312px">
                    <asp:Label ID="Label4" runat="server" Text="BRANCH"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="branch" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="width: 312px">
                    <asp:Label ID="Label5" runat="server" Text="CONTACT"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="contact" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="width: 312px">
                    <asp:Label ID="Label6" runat="server" Text="USERNAME"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="email" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="width: 312px">
                    <asp:Label ID="Label7" runat="server" Text="BOOK NAME"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="bookname" runat="server" OnSelectedIndexChanged="bookname_SelectedIndexChanged" AutoPostBack="True">
                    </asp:DropDownList>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </td>
            </tr>
            <tr>
                <td style="width: 312px">
                    <asp:Label ID="Label8" runat="server" Text="QUANTITY IN STORE"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="quant" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="width: 312px">
                    <asp:Label ID="Label9" runat="server" Text="AVAILABLE  QUANTITY AFTER ISSUEING"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="availquant" runat="server" ></asp:TextBox>
                </td>
            </tr>
            </table>

    </div>
    <div>
     <center>  
         <br />
         <br />
         <asp:Button ID="Button1" runat="server" Text="ISSUE" OnClick="Button1_Click" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Button ID="Button13" runat="server" OnClick="Button13_Click" Text="updation after issueing" />
         <br />
         <br />
         <asp:Literal ID="Literal1" runat="server"></asp:Literal>
&nbsp;</center> 
    </div>
</asp:Content>
