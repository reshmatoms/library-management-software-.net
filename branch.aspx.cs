﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace project1
{
    public partial class branch1 : System.Web.UI.Page
    {
        string str = "Data Source=DESKTOP-J98SMJN\\SQLEXPRESS;Initial Catalog=project1;Integrated Security=True";
        admindataclassesDataContext dc;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Buttonpub_Click(object sender, EventArgs e)
        {
            dc = new admindataclassesDataContext(str);
            branch b = new branch();
            b.bid = Convert.ToInt32(bchid.Text);
            b.bname = bchname.Text;
            DateTime now = System.DateTime.Now;
            b.entrydate = Convert.ToDateTime(now);
            dc.branches.InsertOnSubmit(b);
            dc.SubmitChanges();
            var query = (from x in dc.branches select new { x.bid, x.bname });
            GridView1.DataSource = query;
            GridView1.DataBind();
            RequiredFieldValidator1.Enabled = true;
            RequiredFieldValidator2.Enabled = true;
        }

        protected void Button11_Click(object sender, EventArgs e)
        {
            dc = new admindataclassesDataContext(str);
            GridView1.DataSource = dc.branches.ToList();
            GridView1.DataBind();
        }
    }
}