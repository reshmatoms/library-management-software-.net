﻿<%@ Page Title="" Language="C#" MasterPageFile="~/nestedadmin.master" AutoEventWireup="true" CodeBehind="book.aspx.cs" Inherits="project1.book" %>
<asp:Content ID="Content1" ContentPlaceHolderID="e" runat="server">
    <div style="background-color:#6b5c5c;">
       
    <center>
            <asp:Label ID="bookhead" runat="server" Text="ADD NEW BOOK" Font-Bold="True" Font-Size="Large" ForeColor="#FFFFCC"></asp:Label>

        </center>
        </div>
        <table class="auto-style1">
            <tr>
                <td colspan="2">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 317px">
                    <asp:Label ID="Label2" runat="server" Text="BOOKNAME"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="bukname" runat="server" OnTextChanged="bukname_TextChanged"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="width: 317px">
                    <asp:Label ID="Label3" runat="server" Text="AUTHOR"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="auth" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="width: 317px">
                    <asp:Label ID="Label4" runat="server" Text="PRICE"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="pice" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="width: 317px">&nbsp;<asp:Label ID="Label5" runat="server" Text="PUBLICATION NAME"></asp:Label>
                    &nbsp;</td>
                <td>
                    <asp:DropDownList ID="pubname" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td style="width: 317px">
                    <asp:Label ID="Label6" runat="server" Text="BRANCH"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="bchname" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td style="width: 317px">
                    <asp:Label ID="Label7" runat="server" Text="QUANTITY"></asp:Label></td>
                <td>
                    <asp:TextBox ID="quant" runat="server"></asp:TextBox>
                </td>
            </tr>
        </table>

    <center>
        <asp:Button ID="Button13" runat="server" Text="view" />
        <br />
        <br />
        <asp:Button ID="Button1" runat="server" Text="ADD BOOK" OnClick="Button1_Click"></asp:Button>
        &nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button ID="Button12" runat="server" OnClick="Button12_Click" Text="UPDATE BOOKS" />
&nbsp;
        <asp:Button ID="Button11" runat="server" OnClick="Button11_Click" Text="VIEW BOOKS" />
        <br />
        <br />
        <br />
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" OnRowDeleting="GridView1_RowDeleting" DataKeyNames="bkid">
            <Columns>
                <%--<asp:TemplateField HeaderText="BOOK ID">
                    <ItemTemplate>
                        <asp:Label ID="bid" runat="server" Text='<%#Eval("bkid") %>'></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:Label ID="BID" runat="server" Text='<%#Eval("bkid") %>'></asp:Label>
                    </EditItemTemplate>
                </asp:TemplateField>--%>
                <asp:TemplateField HeaderText="BOOK NAME">
                    <ItemTemplate>
                                  <asp:Label ID="Label2" runat="server" Font-Bold="True" Text='<%#Eval("bkname") %>'></asp:Label>
                              </ItemTemplate>
                              <EditItemTemplate>
                                   <asp:TextBox ID="bukname" runat="server" Width="120px" Text='<%#Eval("bkname") %>' ></asp:TextBox>
                              </EditItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="AUTHOR" >
                    <ItemTemplate>
                                  <asp:Label ID="Label3" runat="server" Font-Bold="True" Text='<%#Eval("author") %>'></asp:Label>
                              </ItemTemplate>
                              <EditItemTemplate>
                                   <asp:TextBox ID="auth" runat="server" Width="120px" Text='<%#Eval("author") %>' ></asp:TextBox>
                              </EditItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="PRICE">
                    <ItemTemplate>
                     <asp:Label ID="Label4" runat="server" Font-Bold="True" Text='<%#Eval("price") %>'></asp:Label>
                              </ItemTemplate>
                              <EditItemTemplate>
                                   <asp:TextBox ID="pice" runat="server" Width="120px" Text='<%#Eval("price") %>' ></asp:TextBox>
                              </EditItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="PUBLICATION NAME">
                    <ItemTemplate>
                        <asp:Label ID="Label5" runat="server" Text='<%#Eval("publication") %>'></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:DropDownList ID="pubname" runat="server" Text='<%#Eval("publication") %>'>
                    </asp:DropDownList>
                    </EditItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField HeaderText="BRANCH NAME">
                     <ItemTemplate>
                          <asp:Label ID="Label6" runat="server" Text='<%#Eval("branch") %>'></asp:Label>
                     </ItemTemplate>
                     <EditItemTemplate>
                          <asp:DropDownList ID="bchname" runat="server" Text='<%#Eval("branch") %>'>
                    </asp:DropDownList>
                     </EditItemTemplate>
                 </asp:TemplateField>
                <asp:TemplateField HeaderText="QUANTITY" >
                    <ItemTemplate>
                        <asp:Label ID="Label7" runat="server" Text='<%#Eval("availquant") %>'></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="quant" runat="server" Text='<%#Eval("availquant") %>' ></asp:TextBox>
                    </EditItemTemplate>
                </asp:TemplateField>
                
                <asp:CommandField ShowDeleteButton="True" />
            </Columns>
        </asp:GridView>
    </center>
</asp:Content>
